import stat
import traceback
from collections import OrderedDict

import click
import gi
import sys

from io import StringIO

from GuiBuilder import GuiBuilder, _run

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

WIDGET_VALUETRETRIEVAL_MAP = {
    Gtk.SpinButton: lambda widget: widget.get_value(),
    Gtk.Entry: lambda widget: widget.get_text(),
    Gtk.FileChooserButton: lambda widget: widget.get_filename(),
    Gtk.Scale: lambda widget: widget.get_value(),
    Gtk.CheckButton: lambda widget: widget.get_active(),
    Gtk.Switch: lambda widget: widget.get_active(),
    Gtk.ComboBoxText: lambda widget: widget.get_active_text(),
}


def none_defaults(x, replace=-1):
    return replace if x is None else x


TYPE_WIDGET_MAP = {
    click.types.IntParamType:
        (Gtk.SpinButton,
         lambda widget, param: widget.set_adjustment(Gtk.Adjustment(value=none_defaults(param['default'], replace=0),
                                                                    lower=none_defaults(-2 ** 32),
                                                                    upper=none_defaults(2 ** 32),
                                                                    step_incr=1))),

    click.types.INT:
        (Gtk.SpinButton,
         lambda widget, param: widget.set_adjustment(Gtk.Adjustment(value=none_defaults(param['default'], replace=0),
                                                                    lower=none_defaults(-2 ** 32),
                                                                    upper=none_defaults(2 ** 32),
                                                                    step_incr=1))),

    click.types.FloatParamType:
        (Gtk.SpinButton,
         lambda widget, param: widget.set_adjustment(Gtk.Adjustment(value=none_defaults(param['default'], replace=0),
                                                                    lower=none_defaults(-2 ** 32),
                                                                    upper=none_defaults(2 ** 32),
                                                                    step_incr=0.01))),

    click.types.FLOAT:
        (Gtk.SpinButton,
         lambda widget, param: widget.set_adjustment(Gtk.Adjustment(value=none_defaults(param['default'], replace=0),
                                                                    lower=none_defaults(-2 ** 32),
                                                                    upper=none_defaults(2 ** 32),
                                                                    step_incr=0.01))),

    click.types.IntRange:
        (Gtk.Scale,
         lambda widget, param: [widget.set_adjustment(Gtk.Adjustment(value=none_defaults(param['default'], replace=0),
                                                                     lower=none_defaults(param['_param'].type.min,
                                                                                         replace=-2 ** 16),
                                                                     upper=none_defaults(param['_param'].type.max,
                                                                                         replace=2 ** 16))),
                                widget.set_digits(0)]),

    click.types.BoolParamType:
        (Gtk.CheckButton,
         lambda widget, param: widget.set_active(param['default'])),

    click.types.BOOL:
        (Gtk.CheckButton,
         lambda widget, param: widget.set_active(param['default'])),

    'is_flag':
        (Gtk.Switch,
         lambda widget, param: widget.set_active(param['default'])),

    click.types.Choice:
        (Gtk.ComboBoxText,
         lambda widget, param: [widget.append(None, t) for t in param['_param'].type.choices]),

    click.types.StringParamType:
        (Gtk.Entry,
         lambda widget, param: widget.set_text(param['default'] if param['default'] else "")),

    click.types.Tuple:
        (Gtk.Entry,
         lambda widget, param: widget.set_text(param['default'] if param['default'] else "")),

    click.types.Path:
        (Gtk.FileChooserButton,
         lambda widget, param: [
             widget.set_action(Gtk.FileChooserAction.SAVE) if param['_param'].name == 'file' and param[
                 '_param'].writable else
             widget.set_action(Gtk.FileChooserAction.CREATE_FOLDER) if param['_param'].name == 'directory' and param[
                 '_param'].writable else
             widget.set_action(Gtk.FileChooserAction.SELECT_FOLDER) if param['_param'].name == 'directory' and param[
                 '_param'].readable else
             widget.set_action(Gtk.FileChooserAction.OPEN)
         ]),

    click.types.File:
        (Gtk.FileChooserButton,
         lambda widget, param: [
             widget.set_action(Gtk.FileChooserAction.SAVE) if param['_param'].type.mode in {'w', 'wt', 'wb', 'a'} else
             widget.set_action(Gtk.FileChooserAction.OPEN)
         ])
}
if hasattr(click.types, 'FloatRamge'):
    TYPE_WIDGET_MAP[click.types.FloatRange] = \
        (Gtk.Scale,
         lambda widget, param: widget.set_adjustment(Gtk.Adjustment(value=none_defaults(param['default'], replace=0),
                                                                    lower=none_defaults(param['_param'].type.min,
                                                                                        replace=-1),
                                                                    upper=none_defaults(param['_param'].type.max,
                                                                                        replace=512)))),


class ClickGtkWindow(Gtk.Window):
    def __init__(self, title: str, structure: dict):
        Gtk.Window.__init__(self, title=title)
        notebook = Gtk.Notebook()
        self.add(notebook)

        def _set_margin(widget, margin):
            widget.set_margin_left(margin)
            widget.set_margin_top(margin)
            widget.set_margin_right(margin)
            widget.set_margin_bottom(margin)

        def labelify(name):
            return " ".join(name.split('_')).strip().title()

        def get_values(widgets: dict):
            result = OrderedDict()
            for name, widget in widgets.items():
                if name == 'help':
                    continue
                get_value = WIDGET_VALUETRETRIEVAL_MAP[widget.__class__]
                result[name] = get_value(widget)
            return result

        def mkcommand(cmd, ano):
            arg_widgets = OrderedDict()
            opt_widgets = OrderedDict()
            arguments = ano['arguments']
            options = ano['options']
            num_arguments = len(arguments)
            num_options = len(options) - 1  # - help option

            # required arguments
            argument_frame = Gtk.Frame(label="Arguments")
            argument_grid = Gtk.Grid()
            argument_grid.set_row_spacing(5)
            argument_grid.set_column_spacing(20)
            argument_grid.expand = True
            for row, (argument, info) in enumerate(arguments.items()):
                label, widget = mkarguments(argument, info)
                arg_widgets[argument] = widget
                if label:
                    argument_grid.attach(label, 0, row, 1, 1)
                    argument_grid.attach_next_to(widget, label, Gtk.PositionType.RIGHT, 1, 1)

            # options
            option_grid = Gtk.Grid()
            option_grid.set_row_spacing(5)
            option_grid.set_column_spacing(20)
            option_grid.expand = True
            for row, (option, info) in enumerate(options.items()):
                label, widget = mkoptions(option, info)
                opt_widgets[option] = widget
                if label:
                    option_grid.attach(label, 0, row, 1, 1)
                    option_grid.attach_next_to(widget, label, Gtk.PositionType.RIGHT, 1, 1)
            options_expander = Gtk.Expander(label="Options")
            options_expander.add(option_grid)

            # "Run" button
            button = Gtk.Button(label=f"→ Run ({cmd.name})")

            def _run_and_catch_exceptions(cmd, args, opts):
                # redirect stdout and stderr to buffers
                stdout = sys.stdout
                stderr = sys.stderr
                stdout_capture = StringIO()
                stderr_capture = StringIO()
                sys.stdout = stdout_capture
                sys.stderr = stderr_capture
                try:
                    result = _run(cmd, args, opts)
                    stdout_msg = stdout_capture.getvalue()

                    if result is not None or stdout_msg:
                        dialog = Gtk.MessageDialog(self, 0,
                                                   Gtk.MessageType.INFO,
                                                   Gtk.ButtonsType.OK,
                                                   "Results")

                        if result is None and stdout_msg.count('\n') < 2 and len(stdout_msg) <= 80:
                            result = stdout_msg
                            stdout_msg = None

                        dialog.format_secondary_text(f"{result}")

                        message_area = dialog.get_message_area()
                        if stdout_msg:
                            traceback_expander = Gtk.Expander(label='Output')
                            message_area.add(traceback_expander)
                            textview = Gtk.TextView()
                            traceback_expander.add(textview)
                            textbuffer = Gtk.TextBuffer()
                            textbuffer.set_text(stdout_msg)
                            textview.set_buffer(textbuffer)
                            message_area.show_all()

                        dialog.run()
                        dialog.destroy()

                    sys.stdout = stdout
                    sys.stdout.write(stdout_capture.getvalue())
                except Exception as e:
                    traceback.print_exc()
                    traceback_msg = stderr_capture.getvalue()
                    stderr.write(traceback_msg)

                    dialog = Gtk.MessageDialog(transient_for=self, flags=0,
                                               message_type=Gtk.MessageType.ERROR,
                                               buttons=Gtk.ButtonsType.CANCEL,
                                               text="An error occured")
                    dialog.format_secondary_text(f"{e}")

                    if traceback_msg:
                        message_area = dialog.get_message_area()
                        traceback_expander = Gtk.Expander(label='Traceback')
                        message_area.add(traceback_expander)
                        textview = Gtk.TextView()
                        traceback_expander.add(textview)
                        textbuffer = Gtk.TextBuffer()
                        textbuffer.set_text(traceback_msg)
                        textview.set_buffer(textbuffer)
                        message_area.show_all()

                    dialog.run()
                    dialog.destroy()

                    sys.stderr = stderr
                    sys.stderr.write(stderr_capture.getvalue())

            button.connect("clicked", lambda _: _run_and_catch_exceptions(cmd,
                                                                          get_values(arg_widgets),
                                                                          get_values(opt_widgets)))
            _set_margin(button, 15)

            argument_frame.add(argument_grid)
            box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
            box.set_border_width(15)
            description_label = Gtk.Label(label=ano['_cmd'].__doc__)
            description_label.set_line_wrap(True)
            _set_margin(description_label, 15)
            box.add(description_label)
            box.add(Gtk.Separator())
            if num_arguments > 0:
                box.add(argument_frame)
            if num_options > 0:
                box.add(options_expander)
            box.add(button)
            container = Gtk.ScrolledWindow()
            container.add(box)
            return container

        def mkoptions(option, info):
            if option in {'help'}:
                return None, None
            t = info['type']
            label = Gtk.Label(label=labelify(option))
            label.set_xalign(0)
            widgetclass, initf = TYPE_WIDGET_MAP.get(t, (Gtk.Entry, lambda w, p: w.set_text(str(p['default']))))
            widget = widgetclass()
            initf(widget, info)
            widget.set_hexpand(True)
            widget.set_tooltip_text(info['help'])
            _set_margin(widget, 15)
            _set_margin(label, 15)
            return label, widget

        def mkarguments(argument, info):
            t = info['type']
            label = Gtk.Label(label=labelify(argument))
            label.set_xalign(0)
            widgetclass, initf = TYPE_WIDGET_MAP.get(t, (Gtk.Entry, lambda w, p: w.set_text(str(p['default']))))
            widget = widgetclass()
            initf(widget, info)
            widget.set_hexpand(True)
            _set_margin(widget, 15)
            _set_margin(label, 15)
            return label, widget

        def mktab(cmd, ano, notebook):
            command = ano['_cmd']
            page = mkcommand(command, ano)
            cmd_title = Gtk.Label(label=labelify(cmd))
            cmd_title.set_tooltip_text(ano['_cmd'].__doc__)
            notebook.append_page(page, cmd_title)

        def attach_commands(structure, notebook):
            for cmd, ano in structure.items():
                if '_cmd' not in ano:  # subcommand!
                    subnotebook = Gtk.Notebook()
                    notebook.append_page(subnotebook, Gtk.Label(label=labelify(cmd)))
                    attach_commands(ano, subnotebook)
                else:
                    mktab(cmd, ano, notebook)

        attach_commands(structure, notebook)


class GtkBuilder(GuiBuilder):
    def show_gui(self):
        win = ClickGtkWindow(self.title, self.structure)
        win.set_default_size(640, 384)
        win.connect('delete-event', Gtk.main_quit)
        win.show_all()
        Gtk.main()
