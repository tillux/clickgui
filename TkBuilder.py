import stat
import traceback
from collections import OrderedDict

import click
import sys

from io import StringIO

from GuiBuilder import GuiBuilder, _run

import tkinter.ttk as ttk
import tkinter as tki


# https://stackoverflow.com/questions/16188420/python-tkinter-scrollbar-for-frame
class VerticalScrolledFrame(ttk.Frame):
    """A pure Tkinter scrollable frame that actually works!
    * Use the 'interior' attribute to place widgets inside the scrollable frame
    * Construct and pack/place/grid normally
    * This frame only allows vertical scrolling

    """

    def __init__(self, parent, *args, **kw):
        ttk.Frame.__init__(self, parent, *args, **kw)

        # create a canvas object and a vertical scrollbar for scrolling it
        vscrollbar = ttk.Scrollbar(self, orient=tki.VERTICAL)
        vscrollbar.pack(fill=tki.Y, side=tki.RIGHT, expand=tki.FALSE)
        canvas = tki.Canvas(self, bd=0, highlightthickness=0, yscrollcommand=vscrollbar.set)
        canvas.pack(side=tki.LEFT, fill=tki.BOTH, expand=tki.TRUE)
        vscrollbar.config(command=canvas.yview)

        # reset the view
        canvas.xview_moveto(0)
        canvas.yview_moveto(0)

        # create a frame inside the canvas which will be scrolled with it
        self.interior = interior = ttk.Frame(canvas)
        interior_id = canvas.create_window(0, 0, window=interior, anchor=tki.NW)

        # track changes to the canvas and frame width and sync them,
        # also updating the scrollbar
        def _configure_interior(event):
            # update the scrollbars to match the size of the inner frame
            size = (interior.winfo_reqwidth(), interior.winfo_reqheight())
            canvas.config(scrollregion="0 0 %s %s" % size)
            if interior.winfo_reqwidth() != canvas.winfo_width():
                # update the canvas's width to fit the inner frame
                canvas.config(width=interior.winfo_reqwidth())

        interior.bind('<Configure>', _configure_interior)

        def _configure_canvas(event):
            if interior.winfo_reqwidth() != canvas.winfo_width():
                # update the inner frame's width to fill the canvas
                canvas.itemconfigure(interior_id, width=canvas.winfo_width())

        canvas.bind('<Configure>', _configure_canvas)


class Spinbox(ttk.Widget):
    def __init__(self, master, **kw):
        ttk.Widget.__init__(self, master, 'ttk::spinbox', kw)


WIDGET_VALUETRETRIEVAL_MAP = {
    Spinbox: lambda widget: widget.get(),
    ttk.Entry: lambda widget: widget.get_text(),
    ttk.Button: lambda widget: widget.get_filename(),
    ttk.Scale: lambda widget: widget.get_value(),
    ttk.Checkbutton: lambda widget: widget.get_active(),
    ttk.Combobox: lambda widget: widget.get_active_text(),
}


def none_defaults(x, replace=-1):
    return replace if x is None else x


TYPE_WIDGET_MAP = {
    click.types.IntParamType:
        (Spinbox,
         lambda widget, param: widget.configure(**{'value': none_defaults(param['default'], replace=0),
                                                   'from': none_defaults(-2 ** 32),
                                                   'to': none_defaults(2 ** 32),
                                                   'increment': 1})),

    click.types.INT:
        (Spinbox,
         lambda widget, param: widget.configure(**{'value': none_defaults(param['default'], replace=0),
                                                   'from': none_defaults(-2 ** 32),
                                                   'to': none_defaults(2 ** 32),
                                                   'increment': 1})),

    click.types.FloatParamType:
        (Spinbox,
         lambda widget, param: widget.configure(**{'value': none_defaults(param['default'], replace=0),
                                                   'from': none_defaults(-2 ** 32),
                                                   'to': none_defaults(2 ** 32),
                                                   'increment': 0.01})),

    click.types.FLOAT:
        (Spinbox,
         lambda widget, param: widget.configure(**{'value': none_defaults(param['default'], replace=0),
                                                   'from': none_defaults(-2 ** 32),
                                                   'to': none_defaults(2 ** 32),
                                                   'increment': 0.01})),

    click.types.IntRange:
        (ttk.Scale,
         lambda widget, param: widget.configure(**{'value': none_defaults(param['default'], replace=0),
                                                   'from': none_defaults(param['_param'].type.min, replace=-2 ** 16),
                                                   'to': none_defaults(param['_param'].type.max, replace=2 ** 16), })),

    click.types.BoolParamType:
        (ttk.Checkbutton,
         lambda widget, param: widget.configure(variable=tki.IntVar(value=param['default']))),

    click.types.BOOL:
        (ttk.Checkbutton,
         lambda widget, param: widget.configure(variable=tki.IntVar(value=param['default']))),

    'is_flag':
        (ttk.Checkbutton,
         lambda widget, param: widget.configure(variable=tki.IntVar(value=param['default']))),

    click.types.Choice:
        (ttk.Combobox,
         lambda widget, param: widget.configure(values=param['_param'].type.choices)),

    click.types.StringParamType:
        (ttk.Entry,
         lambda widget, param: widget.insert(0, param['default'] if param['default'] else "")),

    click.types.Tuple:
        (ttk.Entry,
         lambda widget, param: widget.insert(0, param['default'] if param['default'] else "")),

    click.types.Path:
        (ttk.Button,
         lambda widget, param: [
             # widget.set_action(ttk.FileChooserAction.SAVE) if param['_param'].name == 'file' and param[
             #     '_param'].writable else
             # widget.set_action(ttk.FileChooserAction.CREATE_FOLDER) if param['_param'].name == 'directory' and param[
             #     '_param'].writable else
             # widget.set_action(ttk.FileChooserAction.SELECT_FOLDER) if param['_param'].name == 'directory' and param[
             #     '_param'].readable else
             # widget.set_action(ttk.FileChooserAction.OPEN)
         ]),

    click.types.File:
        (ttk.Button,
         lambda widget, param: [
             # widget.set_action(ttk.FileChooserAction.SAVE) if param['_param'].type.mode in {'w', 'wt', 'wb', 'a'} else
             # widget.set_action(ttk.FileChooserAction.OPEN)
         ])
}
if hasattr(click.types, 'FloatRamge'):
    TYPE_WIDGET_MAP[click.types.FloatRange] = \
        (ttk.Scale,
         lambda widget, param: widget.configure(**{'value': none_defaults(param['default'], replace=0),
                                                   'from': none_defaults(param['_param'].type.min, replace=-2 ** 16),
                                                   'to': none_defaults(param['_param'].type.max, replace=2 ** 16),
                                                   'digits': 4})),


class ClickTtkWindow():
    def __init__(self, root, title: str, structure: dict):
        frame = ttk.Frame(root)
        frame.pack(fill=tki.BOTH, expand=tki.TRUE)
        notebook = ttk.Notebook(frame, name=title)
        notebook.pack(fill=tki.BOTH, expand=tki.TRUE)

        def labelify(name):
            return " ".join(name.split('_')).strip().lower()

        def get_values(widgets: dict):
            result = OrderedDict()
            for name, widget in widgets.items():
                if name == 'help':
                    continue
                get_value = WIDGET_VALUETRETRIEVAL_MAP[widget.__class__]
                result[name] = get_value(widget)
            return result

        def mkcommand(parent, cmd, ano):
            container = ttk.Frame(parent)
            arg_widgets = OrderedDict()
            opt_widgets = OrderedDict()
            arguments = ano['arguments']
            options = ano['options']
            num_arguments = len(arguments)
            num_options = len(options) - 1  # - help option

            # required arguments
            tki.Grid.columnconfigure(container, 0, weight=1)
            tki.Grid.rowconfigure(container, 0, weight=1)
            argument_frame = ttk.LabelFrame(container, text="Arguments")
            argument_frame.grid(row=0, columnspan=2, sticky=tki.W + tki.E + tki.N + tki.S)

            [tki.Grid.columnconfigure(argument_frame, col, weight=1) for col in range(2)]
            [tki.Grid.rowconfigure(argument_frame, row, weight=1) for row in range(num_arguments)]

            for row, (argument, info) in enumerate(arguments.items()):
                label, widget = mkarguments(argument_frame, argument, info)
                arg_widgets[argument] = widget
                if label:
                    label.grid(row=row, column=0, sticky=tki.W + tki.N)
                    widget.grid(row=row, column=1, sticky=tki.E + tki.N)

            # options
            option_frame = ttk.LabelFrame(container, text="Options")
            option_frame.grid(row=1, columnspan=2, sticky=tki.W + tki.E + tki.N + tki.S)

            [tki.Grid.columnconfigure(option_frame, col, weight=1) for col in range(2)]
            [tki.Grid.rowconfigure(option_frame, row, weight=1) for row in range(num_options)]

            for row, (option, info) in enumerate(options.items()):
                label, widget = mkoptions(option_frame, option, info)
                opt_widgets[option] = widget
                if label:
                    label.grid(row=row, column=0, sticky=tki.W + tki.N)
                    widget.grid(row=row, column=1, sticky=tki.E + tki.N)

            # "Run" button
            button = ttk.Button(container, text=f"→ Run ({cmd.name})")
            button.grid(row=2, column=1, sticky=tki.E + tki.S)

            def _run_and_catch_exceptions(cmd, args, opts):
                # redirect stdout and stderr to buffers
                stdout = sys.stdout
                stderr = sys.stderr
                stdout_capture = StringIO()
                stderr_capture = StringIO()
                sys.stdout = stdout_capture
                sys.stderr = stderr_capture
                try:
                    result = _run(cmd, args, opts)
                    stdout_msg = stdout_capture.getvalue()

                    if result is not None or stdout_msg:
                        dialog = ttk.MessageDialog(self, 0,
                                                   ttk.MessageType.INFO,
                                                   ttk.ButtonsType.OK,
                                                   "Results")

                        if result is None and stdout_msg.count('\n') < 2 and len(stdout_msg) <= 80:
                            result = stdout_msg
                            stdout_msg = None

                        dialog.format_secondary_text(f"{result}")

                        message_area = dialog.get_message_area()
                        if stdout_msg:
                            traceback_expander = ttk.Expander(text='Output')
                            message_area.add(traceback_expander)
                            textview = ttk.TextView()
                            traceback_expander.add(textview)
                            textbuffer = ttk.TextBuffer()
                            textbuffer.set_text(stdout_msg)
                            textview.set_buffer(textbuffer)
                            message_area.show_all()

                        dialog.run()
                        dialog.destroy()

                    sys.stdout = stdout
                    sys.stdout.write(stdout_capture.getvalue())
                except Exception as e:
                    traceback.print_exc()
                    traceback_msg = stderr_capture.getvalue()
                    stderr.write(traceback_msg)

                    dialog = ttk.MessageDialog(transient_for=self, flags=0,
                                               message_type=ttk.MessageType.ERROR,
                                               buttons=ttk.ButtonsType.CANCEL,
                                               text="An error occured")
                    dialog.format_secondary_text(f"{e}")

                    if traceback_msg:
                        message_area = dialog.get_message_area()
                        traceback_expander = ttk.Expander(text='Traceback')
                        message_area.add(traceback_expander)
                        textview = ttk.TextView()
                        traceback_expander.add(textview)
                        textbuffer = ttk.TextBuffer()
                        textbuffer.set_text(traceback_msg)
                        textview.set_buffer(textbuffer)
                        message_area.show_all()

                    dialog.run()
                    dialog.destroy()

                    sys.stderr = stderr
                    sys.stderr.write(stderr_capture.getvalue())

            # button.connect("clicked", lambda _: _run_and_catch_exceptions(cmd,
            #                                                               get_values(arg_widgets),
            #                                                               get_values(opt_widgets)))

            description_label = ttk.Label(text=ano['_cmd'].__doc__)
            # if num_arguments > 0:
            #     box.add(argument_frame)
            # if num_options > 0:
            #     box.add(options_expander)
            # box.add(button)
            # container.add(box)
            [tki.Grid.columnconfigure(container, col, weight=1) for col in range(2)]
            [tki.Grid.rowconfigure(container, row, weight=1) for row in range(2)]
            return container

        def mkoptions(parent, option, info):
            if option in {'help'}:
                return None, None
            t = info['type']
            label = ttk.Label(parent, text=labelify(option))
            widgetclass, initf = TYPE_WIDGET_MAP.get(t, (ttk.Entry, lambda w, p: w.configure(text=str(p['default']))))
            widget = widgetclass(parent)
            initf(widget, info)
            return label, widget

        def mkarguments(parent, argument, info):
            t = info['type']
            label = ttk.Label(parent, text=labelify(argument))
            widgetclass, initf = TYPE_WIDGET_MAP.get(t, (ttk.Entry, lambda w, p: w.configure(text=str(p['default']))))
            widget = widgetclass(parent)
            initf(widget, info)
            return label, widget

        def mktab(cmd, ano, notebook):
            command = ano['_cmd']
            page = mkcommand(notebook, command, ano)
            # cmd_title = ttk.Label(text=labelify(cmd))
            # cmd_title.set_tooltip_text(ano['_cmd'].__doc__)
            # notebook.add(page, cmd_title)
            notebook.add(page)

        def attach_commands(structure, notebook):
            for cmd, ano in structure.items():
                if '_cmd' not in ano:  # subcommand!
                    subnotebook = ttk.Notebook(notebook, name=labelify(cmd))
                    notebook.add(subnotebook)
                    attach_commands(ano, subnotebook)
                else:
                    mktab(cmd, ano, notebook)

        attach_commands(structure, notebook)


class TkBuilder(GuiBuilder):
    def show_gui(self):
        tk_root = tki.Tk()
        win = ClickTtkWindow(tk_root, self.title, self.structure)
        tk_root.mainloop()
        try:
            tk_root.destroy()
        except tki.TclError:
            pass
