import traceback
from collections import OrderedDict
from string import Template

import click
import remi.gui as Html
from remi import start, App

from GuiBuilder import GuiBuilder, _run


class FileChooserButtonHtml5(Html.Input):
    def __init__(self):
        super().__init__(input_type='file')
        javascript_code = Html.Tag()
        javascript_code.type = 'script'
        javascript_code.attributes['type'] = 'text/javascript'
        code = Template("""
        function handleFileSelect(evt) {
            var files = evt.target.files; // FileList object
            var fReader = new FileReader();
            fReader.readAsDataURL(input.files[0]);
            // files is a FileList of File objects. List some properties.
            var output = [];
            for (var i = 0, f; f = files[i]; i++) {
              output.push(escape(f.name));
            }
            // document.getElementById('list').innerHTML = '<ul>' + output.join('') + '</ul>';
            alert(output.join(''));
          }
        
          document.getElementById('$elmntid').addEventListener('change', handleFileSelect, false);
        """).substitute(elmntid=id(self))
        javascript_code.add_child('code', code % {'id': id(self), })
        self.add_child('javascript_code', javascript_code)
        # self.set_on_change_listener(lambda w, e: print(f"{w}: {e}"))

    def get_file(self):
        return self.get_value()


class FileChooserButton(Html.Button):
    def __init__(self, text="Browse ..."):
        super().__init__(text=text)
        self.fileselectionDialog = None
        self.set_on_click_listener(self.open_fileselection_dialog)
        self.files = None

    def open_fileselection_dialog(self, widget):
        self.fileselectionDialog = Html.FileSelectionDialog('File Selection Dialog', 'Select files and folders', False,
                                                            '.')
        self.fileselectionDialog.set_on_confirm_value_listener(self.on_fileselection_dialog_confirm)

        # here is returned the Input Dialog widget, and it will be shown
        self.fileselectionDialog.show(app_instance)

    def on_fileselection_dialog_confirm(self, widget, filelist):
        # a list() of filenames and folders is returned
        if not filelist:
            self.files = None

        self.files = filelist
        num_files = len(filelist)
        str_len = len(filelist[0])
        if num_files == 1:
            self.set_text(filelist[0])
        else:
            self.set_text(', '.join(filelist)[:str_len + 3] + " …")

    def get_files(self):
        return self.files

    def get_file(self):
        if self.files and len(self.files) == 1:
            return self.files[0]
        else:
            return None


class SingleLineInput(Html.Input, Html._MixinTextualWidget):
    def __init__(self, hint='', **kwargs):
        super(SingleLineInput, self).__init__(input_type='text', single_line=True, hint=hint, **kwargs)


WIDGET_VALUETRETRIEVAL_MAP = {
    Html.SpinBox: lambda widget: widget.get_value(),
    SingleLineInput: lambda widget: widget.get_value(),
    Html.Slider: lambda widget: widget.get_value(),
    FileChooserButton: lambda widget: widget.get_file(),
    Html.CheckBox: lambda widget: widget.get_value(),
    Html.DropDown: lambda widget: widget.get_value(),
    # Gtk.Switch: lambda widget: widget.get_active(),
}


def none_defaults(x, replace=-1):
    return replace if x is None else x


TYPE_WIDGET_MAP = {
    click.types.IntParamType:
        (Html.SpinBox,
         lambda widget, param: [widget.set_value(none_defaults(param['default'], replace=0)),
                                widget.attributes.update(
                                    min=str(none_defaults(-2 ** 32)),
                                    max=str(none_defaults(2 ** 32)),
                                    step=str(1)
                                )]),

    click.types.INT:
        (Html.SpinBox,
         lambda widget, param: [widget.set_value(none_defaults(param['default'], replace=0)),
                                widget.attributes.update(
                                    min=str(none_defaults(-2 ** 32)),
                                    max=str(none_defaults(2 ** 32)),
                                    step=str(1)
                                )]),

    click.types.FloatParamType:
        (Html.SpinBox,
         lambda widget, param: [widget.set_value(none_defaults(param['default'], replace=0)),
                                widget.attributes.update(
                                    min=str(none_defaults(-2 ** 32)),
                                    max=str(none_defaults(2 ** 32)),
                                    step=str(0.01)
                                )]),

    click.types.FLOAT:
        (Html.SpinBox,
         lambda widget, param: [widget.set_value(none_defaults(param['default'], replace=0)),
                                widget.attributes.update(
                                    min=str(none_defaults(-2 ** 32)),
                                    max=str(none_defaults(2 ** 32)),
                                    step=str(0.01)

                                )]),

    click.types.IntRange:
        (Html.SpinBox,  # QSlider instead?
         lambda widget, param: [widget.set_value(none_defaults(param['default'], replace=0)),
                                widget.attributes.update(
                                    min=str(none_defaults(param['_param'].type.min, replace=-2 ** 16)),
                                    max=str(none_defaults(param['_param'].type.max, replace=2 ** 16)),
                                    step=str(1)
                                )]),

    click.types.BoolParamType:
        (Html.CheckBox,
         lambda widget, param: widget.set_value(param['default'])),

    click.types.BOOL:
        (Html.CheckBox,
         lambda widget, param: widget.set_value(param['default'])),

    # 'is_flag':
    #     (Gtk.Switch,
    #      lambda widget, param: widget.set_active(param['default'])),

    click.types.Choice:
        (Html.DropDown,
         lambda widget, param: [widget.append(choice) for choice in param['_param'].type.choices]),

    click.types.StringParamType:
        (SingleLineInput,
         lambda widget, param: widget.set_value(param['default'] if param['default'] else "")),

    click.types.Tuple:
        (SingleLineInput,
         lambda widget, param: widget.set_value(param['default'] if param['default'] else "")),

    click.types.Path:
        (FileChooserButton,
         lambda widget, param: None),

    click.types.File:
        (FileChooserButton,
         lambda widget, param: None)
}

global root_structure
global app_instance


class ClickHtmlGui(App):
    def __init__(self, *args):
        super(ClickHtmlGui, self).__init__(*args)

    @staticmethod
    def _build_gui():
        notebook = Html.TabBox()

        def labelify(name):
            return " ".join(name.split('_')).strip().title()

        def get_values(widgets: dict):
            result = OrderedDict()
            for name, widget in widgets.items():
                if name == 'help':
                    continue
                get_value = WIDGET_VALUETRETRIEVAL_MAP[widget.__class__]
                result[name] = get_value(widget)
            return result

        def mkcommand(cmd, ano):
            arg_widgets = OrderedDict()
            opt_widgets = OrderedDict()
            arguments = ano['arguments']
            options = ano['options']
            num_arguments = len(arguments)
            num_options = len(options) - 1  # - help option

            # required arguments
            # argument_frame = QGroupBox()
            # argument_frame.setTitle("Arguments")
            argument_table = Html.Table()
            argument_table.append_from_list([('Arguments', '')], fill_title=True)
            for row, (argument, info) in enumerate(arguments.items()):
                label, widget = mkarguments(argument, info)
                arg_widgets[argument] = widget
                if label:
                    argument_table.append_from_list([(label, widget)])

            # options
            option_table = Html.Table()
            option_table.append_from_list([('Options', '')], fill_title=True)
            for row, (option, info) in enumerate(options.items()):
                label, widget = mkoptions(option, info)
                opt_widgets[option] = widget
                if label:
                    option_table.append_from_list([(label, widget)])
            style_table(option_table)
            style_table(argument_table)

            # options_expander = QGroupBox()
            # # options_expander.setBackgroundRole(QPalette.Window)
            # options_expander.setTitle("Options")
            # options_expander.setLayout(option_grid)

            # "Run" button
            button = Html.Button(f"→ Run ({cmd.name})")

            def _run_and_catch_exceptions(cmd, args, opts):
                try:
                    result = _run(cmd, args, opts)
                    if result is not None:
                        dialog = Html.GenericDialog('Results', f"{result}")
                        dialog.show(app_instance)
                except Exception as e:
                    # print(e, file=stderr)
                    traceback.print_exc()
                    dialog = Html.GenericDialog('An error occured', f"{e}")
                    dialog.show(app_instance)

            # button.clicked.connect(lambda _: _run_and_catch_exceptions(cmd, get_values(arg_widgets), get_values(opt_widgets)))  # self.on_button_clicked)
            button.set_on_click_listener(
                lambda _: _run_and_catch_exceptions(cmd, get_values(arg_widgets), get_values(opt_widgets)))
            box = Html.VBox()
            box.style['margin-top'] = '1em'
            description = ano['_cmd'].__doc__
            if not description:
                description = ""
            description_label = Html.Label(description)
            widgets = [description_label, argument_table, option_table, button]
            if num_arguments == 0:
                widgets.remove(argument_table)
            if num_options == 0:
                widgets.remove(option_table)
            for i, w in enumerate(widgets):
                box.append(w, i)
            return box

        def mkoptions(option, info):
            if option in {'help'}:
                return None, None
            t = info['type']
            label = Html.Label(labelify(option))
            widgetclass, initf = TYPE_WIDGET_MAP.get(t, (SingleLineInput, lambda w, p: w.set_value(str(p['default']))))
            widget = widgetclass()
            initf(widget, info)
            # widget.setToolTip(info['help'])
            return label, widget

        def mkarguments(argument, info):
            t = info['type']
            label = Html.Label(labelify(argument))
            # label.set_xalign(0)
            widgetclass, initf = TYPE_WIDGET_MAP.get(t, (SingleLineInput, lambda w, p: w.set_value(str(p['default']))))
            widget = widgetclass()
            initf(widget, info)
            # widget.set_hexpand(True)
            return label, widget

        def mktab(cmd, ano, notebook):
            command = ano['_cmd']
            page = mkcommand(command, ano)
            cmd_title = Html.Label(labelify(cmd))
            # cmd_title.setToolTip(ano['_cmd'].__doc__)
            notebook.add_tab(page, labelify(cmd), lambda *_: None)

        def attach_commands(structure, notebook):
            for cmd, ano in structure.items():
                if '_cmd' not in ano:  # subcommand!
                    subnotebook = Html.TabBox()
                    notebook.add_tab(subnotebook, labelify(cmd), lambda *_: None)
                    attach_commands(ano, subnotebook)
                    style_notebook(subnotebook)
                else:
                    mktab(cmd, ano, notebook)

        attach_commands(root_structure, notebook)
        style_notebook(notebook)
        return notebook

    def main(self):
        global app_instance
        app_instance = self
        return ClickHtmlGui._build_gui()


def style_notebook(notebook):
    tab_w = 100 / len(notebook._tabs)
    notebook._ul.style['display'] = 'inline-table'
    notebook._ul.style['width'] = '100%'
    notebook._ul.style['margin'] = '0'
    notebook.style['text-align'] = 'center'
    for a, li, holder in notebook._tabs.values():
        holder.style['padding'] = '0'
        li.style['display'] = 'inline'
        li.style['width'] = "%.4f%%" % tab_w


def style_table(table):
    table.style['margin-top'] = '1.5em'
    table.style['margin-bottom'] = '1.5em'
    table.style['padding'] = '1em'
    table.style['width'] = '90%'
    table.style['line-height'] = '1.75em'
    table.style['text-align'] = 'left'
    table.style['background-color'] = '#f9f9f9'
    for i, (kr, tr) in enumerate(table.children.items()):
        tr.style['line-height'] = '1.75em'
        tr.style['height'] = '1.75em'
        # tr.style['background-color'] = '#f7f7f7' if i % 2 == 1 else 'inherit'
        for j, (kc, td) in enumerate(tr.children.items()):
            td.style['text-align'] = 'right' if j % 2 == 1 else 'left'
            td.style['padding'] = '0.75em'
            if j % 2 == 1:
                for ki, child in td.children.items():
                    if child:
                        child.style['margin-right'] = '1.75em'
                        if isinstance(child, Html.Button):
                            continue
                        child.style['padding'] = '0.75em'


class HtmlBuilder(GuiBuilder):
    def __init__(self, title: str, structure: dict):
        super().__init__(title, structure)
        global root_structure  # sad. needed because remi's start method needs a class, not an instance
        root_structure = self.structure

    def show_gui(self):
        chg = ClickHtmlGui
        try:
            start(chg, standalone=True)
        except:
            start(chg, standalone=False)
