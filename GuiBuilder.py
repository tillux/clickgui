from sys import stderr
from math import isclose


class GuiBuilder:
    def __init__(self, title: str, structure: dict):
        self.title = title
        self.structure = structure

    def show_gui(self):
        pass


def _run(cmd, args, opts):
    print(f"Executing {cmd.name}({args.values()}, {opts})", file=stderr)
    params = list(args.values())

    def quote(s):
        if isinstance(s, str) and " " in s:
            return f"\"{s}\""
        elif isinstance(s, float) and isclose(s, round(s)):  # fixme temporary hack
            return int(s)
        else:
            return s

    for opt, value in opts.items():
        if value:
            params += [f"--{opt.replace('_', '-')}"]
            params += [f"{quote(value)}"]
    result = cmd.main(args=params, standalone_mode=False, prog_name=cmd.name)
    return result
