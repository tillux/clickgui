## Requirements
 - click: `pip install click` / `conda install -c conda-forge click`
 - Depending on the backend you wish to use, install any of:
     - GTK: Install [pygobject](https://pygobject.readthedocs.io/en/latest/getting_started.html)[^conda installation].
     - QT: Install PyQt5 via `pip install PyQt5`
     - HTML: Install remi via `pip install remi` (might not be needed in future versions anymore)

*(Note that the GUI toolkit gets imported on demand, so only one of the GUI toolkit dependencies needs to be met)*


## Usage
`python clickgui.py python.module.path.to.clickcommand -b BACKEND` where `clickcommand` is a python function decorated by `@click.command` or `@click.group` and `BACKEND` is either of `gtk`, `qt` or `html`.

### Decorators
For convenience reasons, the following decorators may be used:

  - The `clickgui` decorator wraps a `click.command` so that when the corresponding file/module gets interpreted, a GUI will be built and shown. Note that there may only be one such decorator in a module *and* that a module thus decorated loses its command line interface. In other words: Only use the decorator if you wish to provide a GUI only.
  
  - The `clickguioption` decorator injects a `--mkgui` option into the click cli (not yet implemented).

## Examples
Since `clickgui` has a command line interface defined through click, it can create a GUI for itself:

  - HTML: `python clickgui.py clickgui.mkclickgui -b html`
  - GTK: `python clickgui.py clickgui.mkclickgui -b gtk`
  - QT: `python clickgui.py clickgui.mkclickgui -b qt`

For more examples, check the *examples* subdirectory.

[^conda installation]: If in a virtual environment (e.g. conda), you can use prebuilt packages from the `conda-forge` channel:
 `conda install -c conda-forge pygobject`
 or use `pip install git+https://git.gnome.org/browse/pygobject`.
 If you get errors about missing typelibs (such as `Gtk-3.0.typelib`), you might have to create symlinks to them in your conda environment,
 e.g. `ln -s /usr/lib/girepository-1.0/* /path/to/conda/envs/SOME_ENV/lib/girepository/`
  and/or e.g. `ln -s /usr/lib/x86_64-linux-gnu/girepository-1.0/* /path/to/conda/envs/SOME_ENV/lib/girepository/`.

 When building pygobject from source (on Ubuntu 16.04), make sure the following packages are installed:
 
  - autotools-dev
  - libffi-dev
  - python-gobject-dev
  - libgirepository1.0-dev
  - python3-cairo-dev
  
  - `conda install pycairo glib`