import os
import sys
import traceback
from collections import OrderedDict

import click
from PyQt5.QtCore import QObject  # needed for avoiding import errors / version clash of Qt4 and Qt5
from PyQt5.QtWidgets import QSpinBox, QLineEdit, QCheckBox, QLabel, QSlider, QTabWidget, QDoubleSpinBox, QWidget, QComboBox, QPushButton, QFrame, QGridLayout, QScrollArea, QVBoxLayout, QApplication, QFileDialog, QMessageBox, QGroupBox

from GuiBuilder import GuiBuilder, _run


class QFileChooserButton(QPushButton):
    def __init__(self, text=None):
        super().__init__()
        if text:
            self.setText(text)
        self.clicked.connect(lambda _: self.__show_dialog())
        self._file = None

    def file(self):
        return self._file

    def __show_dialog(self):
        file, _ = QFileDialog.getOpenFileName()
        self.setText(os.path.basename(file))
        self._file = file


WIDGET_VALUETRETRIEVAL_MAP = {
    QSpinBox: lambda widget: widget.value(),
    QLineEdit: lambda widget: widget.text(),
    QSlider: lambda widget: widget.value(),
    QCheckBox: lambda widget: widget.checkState(),
    QFileChooserButton: lambda widget: widget.file(),
    QComboBox: lambda widget: widget.currentText(),
    # Gtk.Switch: lambda widget: widget.get_active(),
}


def none_defaults(x, replace=-1):
    return replace if x is None else x


TYPE_WIDGET_MAP = {
    click.types.IntParamType:
        (QSpinBox,
         lambda widget, param: widget.setValue(none_defaults(param['default'], replace=0))),

    click.types.INT:
        (QSpinBox,
         lambda widget, param: widget.setValue(none_defaults(param['default'], replace=0))),

    click.types.FloatParamType:
        (QDoubleSpinBox,
         lambda widget, param: widget.setValue(none_defaults(param['default'], replace=0))),

    click.types.FLOAT:
        (QDoubleSpinBox,
         lambda widget, param: widget.setValue(none_defaults(param['default'], replace=0))),

    click.types.IntRange:
        (QSpinBox,  # QSlider instead?
         lambda widget, param: widget.setValue(none_defaults(param['default'], replace=0))),
    # widget.set_adjustment(Gtk.Adjustment(value=none_defaults(param['default'], replace=0),
    #                                      lower=none_defaults(param['_param'].type.min, replace=-1),
    #                                      upper=none_defaults(param['_param'].type.max, replace=512)))),

    click.types.BoolParamType:
        (QCheckBox,
         lambda widget, param: widget.setCheckState(param['default'])),

    click.types.BOOL:
        (QCheckBox,
         lambda widget, param: widget.setCheckState(param['default'])),

    # 'is_flag':
    #     (Gtk.Switch,
    #      lambda widget, param: widget.set_active(param['default'])),

    click.types.Choice:
        (QComboBox,
         lambda widget, param: widget.addItems(param['_param'].type.choices)),

    click.types.StringParamType:
        (QLineEdit,
         lambda widget, param: widget.setText(param['default'] if param['default'] else "")),

    click.types.Tuple:
        (QLineEdit,
         lambda widget, param: widget.setText(param['default'] if param['default'] else "")),

    click.types.Path:
        (QFileChooserButton,  # TODO: set_action(gtk.FILE_CHOOSER_ACTION_SELECT_FOLDER) for folders
         lambda widget, param: widget.setText("Open ...")),

    click.types.File:
        (QFileChooserButton,
         lambda widget, param: widget.setText("Open ..."))
}


# if hasattr(click.types, 'FloatRamge'):
#     TYPE_WIDGET_MAP[click.types.FloatRange] = \
#         (QSlider,
#          lambda widget, param: widget.set_adjustment(Gtk.Adjustment(value=none_defaults(param['default'], replace=0),
#                                                                     lower=none_defaults(param['_param'].type.min, replace=-1),
#                                                                     upper=none_defaults(param['_param'].type.max, replace=512)))),


class ClickQtWindow(QWidget):
    def __init__(self, title: str, structure: dict):
        QWidget.__init__(self)
        layout = QVBoxLayout()
        self.setLayout(layout)
        notebook = QTabWidget()
        layout.addWidget(notebook)

        def labelify(name):
            return " ".join(name.split('_')).strip().title()

        def get_values(widgets: dict):
            result = OrderedDict()
            for name, widget in widgets.items():
                if name == 'help':
                    continue
                get_value = WIDGET_VALUETRETRIEVAL_MAP[widget.__class__]
                result[name] = get_value(widget)
            return result

        def mkcommand(cmd, ano):
            arg_widgets = OrderedDict()
            opt_widgets = OrderedDict()
            arguments = ano['arguments']
            options = ano['options']
            num_arguments = len(arguments)
            num_options = len(options) - 1  # - help option

            # required arguments
            argument_frame = QGroupBox()
            argument_frame.setTitle("Arguments")
            argument_grid = QGridLayout()
            for row, (argument, info) in enumerate(arguments.items()):
                label, widget = mkarguments(argument, info)
                arg_widgets[argument] = widget
                if label:
                    argument_grid.addWidget(label, row, 0, 1, 1)
                    argument_grid.addWidget(widget, row, 1, 1, 1)

            # options
            option_grid = QGridLayout()
            for row, (option, info) in enumerate(options.items()):
                label, widget = mkoptions(option, info)
                opt_widgets[option] = widget
                if label:
                    option_grid.addWidget(label, row, 0, 1, 1)
                    option_grid.addWidget(widget, row, 1, 1, 1)
            options_expander = QGroupBox()
            # options_expander.setBackgroundRole(QPalette.Window)
            options_expander.setTitle("Options")
            options_expander.setLayout(option_grid)

            # "Run" button
            button = QPushButton()
            button.setText(f"→ Run ({cmd.name})")

            def _run_and_catch_exceptions(cmd, args, opts):
                try:
                    result = _run(cmd, args, opts)
                    if result is not None:
                        dialog = QMessageBox(self)
                        dialog.setText(str(result))
                        dialog.setStandardButtons(QMessageBox.Ok)
                        dialog.exec()
                except Exception as e:
                    # print(e, file=stderr)
                    traceback.print_exc()
                    dialog = QMessageBox(self)
                    dialog.setText(str(e))
                    dialog.setStandardButtons(QMessageBox.Ok)
                    dialog.exec()

            button.clicked.connect(lambda _: _run_and_catch_exceptions(cmd, get_values(arg_widgets), get_values(opt_widgets)))  # self.on_button_clicked)

            argument_frame.setLayout(argument_grid)
            box = QVBoxLayout()
            description_label = QLabel(ano['_cmd'].__doc__)
            description_label.setWordWrap(True)
            box.addWidget(description_label)
            if num_arguments > 0:
                box.addWidget(argument_frame)
            if num_options > 0:
                box.addWidget(options_expander)
            box.addStretch()
            box.addWidget(button)
            scroll = QScrollArea()
            scroll.setWidgetResizable(True)
            inner = QFrame(scroll)
            inner.setLayout(box)
            scroll.setWidget(inner)
            return scroll

        def mkoptions(option, info):
            if option in {'help'}:
                return None, None
            t = info['type']
            label = QLabel(labelify(option))
            widgetclass, initf = TYPE_WIDGET_MAP.get(t, (QLineEdit, lambda w, p: w.setText(str(p['default']))))
            widget = widgetclass()
            initf(widget, info)
            widget.setToolTip(info['help'])
            return label, widget

        def mkarguments(argument, info):
            t = info['type']
            label = QLabel(labelify(argument))
            # label.set_xalign(0)
            widgetclass, initf = TYPE_WIDGET_MAP.get(t, (QLineEdit, lambda w, p: w.setText(str(p['default']))))
            widget = widgetclass()
            initf(widget, info)
            # widget.set_hexpand(True)
            # _set_margin(widget, 15)
            # _set_margin(label, 15)
            return label, widget

        def mktab(cmd, ano, notebook):
            command = ano['_cmd']
            page = mkcommand(command, ano)
            cmd_title = QLabel(labelify(cmd))
            cmd_title.setToolTip(ano['_cmd'].__doc__)
            notebook.addTab(page, labelify(cmd))

        def attach_commands(structure, notebook):
            for cmd, ano in structure.items():
                if '_cmd' not in ano:  # subcommand!
                    subnotebook = QTabWidget()
                    notebook.addTab(subnotebook, labelify(cmd))
                    attach_commands(ano, subnotebook)
                else:
                    mktab(cmd, ano, notebook)

        attach_commands(structure, notebook)


class QtBuilder(GuiBuilder):
    def show_gui(self):
        app = QApplication(sys.argv)
        win = ClickQtWindow(self.title, self.structure)
        win.show()
        sys.exit(app.exec_())
