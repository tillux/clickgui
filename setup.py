from setuptools import setup, find_packages

setup(
    name="clickgui",
    author='Till Hartmann',
    author_email='till.hartmann@tu-dortmund.de',
    license='MIT',
    version="0.1",
    packages=find_packages(),
)
