import importlib
import signal
import sys
from collections import OrderedDict

import click
from click import Context


def keyboard_interrupt_handler(signal, frame):
    exit(0)


signal.signal(signal.SIGINT, keyboard_interrupt_handler)


def _guess_backend():
    """Guesses the preferred backend for this platform / desktop environment / window manager.
    If unable to do so, check gtk > qt > html > tk for availability (in that order) and return
    the first hit.

    Returns:
        The preferred backend, if any; the first available backend otherwise.
    """
    import platform
    os_name = platform.system()

    def _get_available_backends():
        available_backends = set()
        try:
            from QtBuilder import QtBuilder
            available_backends.add('qt')
        except:
            pass
        try:
            from GtkBuilder import GtkBuilder
            available_backends.add('gtk')
        except:
            pass
        try:
            from HtmlBuilder import HtmlBuilder
            available_backends.add('html')
        except:
            pass
        try:
            from TkBuilder import TkBuilder
            available_backends.add('tk')
        except:
            pass
        return available_backends

    preferred_backend = 'qt'  # for windows and macos
    if os_name == 'Linux':  # under Linux, choose backend which fits current DE/wmgr best.
        import os
        desktop = os.environ['XDG_CURRENT_DESKTOP']
        if desktop in {'GNOME', 'XFCE', 'LXDE', 'Unity', 'X-Cinnamon'}:
            preferred_backend = 'gtk'
        elif desktop in {'KDE'}:
            preferred_backend = 'qt'
        elif desktop == '':
            from shutil import which
            if which('wmctrl'):
                import subprocess
                wmgr = subprocess.Popen(['wmctrl', '-m'], stdout=subprocess.PIPE).communicate()[0].strip().decode().split('\n')[0]
                wmgr = wmgr[wmgr.index(':') + 2:].lower()
                if wmgr in {'gnome shell', 'metacity', 'mutter', 'unity'}:
                    preferred_backend = 'gtk'
                elif wmgr in {'kde-plasma', 'kde plasma', 'plasma', 'kde', 'kwin'}:
                    preferred_backend = 'qt'

    # if the preferred backend is available, use that
    # if it isn't available, check in order of gtk > qt > html > tk
    available_backends = _get_available_backends()
    if preferred_backend in available_backends:
        return preferred_backend
    elif 'gtk' in available_backends:
        return 'gtk'
    elif 'qt' in available_backends:
        return 'qt'
    elif 'html' in available_backends:
        return 'html'
    else:
        return 'tk'


def get_structure(func):
    """Extract the command structure of a `click.Command` or `click.Group` (does not support `click.MultiCommand`, yet

    Args:
        func (click.Command):

    Returns:

    """
    commands = OrderedDict()
    with Context(func.__class__) as ctx:
        if not isinstance(func, click.Group):
            g = click.Group()
            g.add_command(func)
            func = g

        for cmd in [func.get_command(ctx, x) for x in func.commands]:
            if isinstance(cmd, click.Group):
                substructure = get_structure(cmd)
                commands[cmd.name] = substructure
            else:
                commands[cmd.name] = dict(_cmd=cmd)
                params = cmd.get_params(ctx)
                for param in params:
                    case = "arguments" if isinstance(param, click.Argument) \
                        else "options" if isinstance(param, click.Option) \
                        else "other"
                    if case not in commands[cmd.name]:
                        commands[cmd.name][case] = OrderedDict()
                    commands[cmd.name][case][param.name] = dict(
                        _param=param,
                        name=param.name,
                        hname=param.human_readable_name,
                        type_name=param.param_type_name,
                        type=param.type.__class__,
                        default=param.default if hasattr(param, 'default') else param.get_default(ctx),
                        nargs=param.nargs,
                    )
                    if hasattr(param, 'gui_name'):
                        commands[cmd.name][case][param.name]['gname'] = param.gui_name
                    if isinstance(param, click.Option):
                        commands[cmd.name][case][param.name]['is_flag'] = param.is_flag
                        commands[cmd.name][case][param.name]['help'] = param.help
    return commands


@click.command()
@click.argument('command', type=click.types.STRING, nargs=1)
@click.option('-p', '--package-path', type=click.types.STRING, default=None,
              help="If given, `importlib.import_module` will be called with the keyword arg `package=package_path`.")
@click.option('-b', '--backend', type=click.types.Choice(['gtk', 'qt', 'html', 'tk']),
              help="GUI toolkit to be used for building the interface.")
def mkclickgui(command, package_path=None, backend=None, **kwargs):
    """Build an interface for any importable click cli application or command/group.
    """
    if backend is None:
        backend = _guess_backend()

    if '.' in command:
        pkg, cmd = command.rsplit('.', 1)
    else:
        pkg, cmd = command, command
    if package_path:
        sys.path.insert(0, package_path)
    module = importlib.import_module(pkg, package=package_path)
    if hasattr(module, cmd):
        cmd = getattr(module, cmd)
    else:
        cmd = module

    _mkclickgui(cmd, backend)


def _mkclickgui(command, backend='html'):
    structure = get_structure(command)
    if backend == 'gtk':
        from GtkBuilder import GtkBuilder
        builder = GtkBuilder(f"{command.name}", structure)
    elif backend == 'qt':
        from QtBuilder import QtBuilder
        builder = QtBuilder(f"{command.name}", structure)
    elif backend == 'html':
        from HtmlBuilder import HtmlBuilder
        builder = HtmlBuilder(f"{command.name}", structure)
    elif backend == 'tk':
        from TkBuilder import TkBuilder
        builder = TkBuilder(f"{command.name}", structure)
    else:
        from GtkBuilder import GtkBuilder
        builder = GtkBuilder(f"{command.name}", structure)
    builder.show_gui()


def clickgui(*args, backend='html', **kwargs):
    def mkgui(func, backend=backend):
        return _mkclickgui(func, backend=backend)

    return mkgui


def clickguioption(*param_decls, **attrs):
    def decorator(f):
        return click.option(*(param_decls or ('--mkgui',)), **attrs)(f)

    return decorator


if __name__ == "__main__":
    mkclickgui()
